class Contact < ApplicationRecord
  belongs_to :user
  has_many :addresses, dependent: :destroy

  validates :name, :user, presence: true
  accepts_nested_attributes_for :addresses, reject_if: :all_blank, allow_destroy: true

end
